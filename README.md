# FIDELIO eService guide

                    FIDELIO eService developer guide
                    2017-11-17, kahlo@adesso.de

## Introduction

This repository contains the source to build the FIDELIO eService component using maven.

### Download

> Get the latest sources here https://gitlab.com/adessoAG/FIDELIO/eService.

## Installation
						
	How to install and setup the FIDELIO server component.
 
 	Prerequisites: JRE8, Tomcat 7+, eID server, Redis (if used)
 
 	- install java
 	- generate passwords for keystores
 		
 		openssl rand -base64 24
 	
 	- create client keypair

 		KEYTOOL=$JAVA/bin/keytool
		$KEYTOOL -genkeypair -v -alias $SIGNKEYNAME -keystore $KEYSTORE -keyalg RSA -keysize 4096 \
 			-validity 1150 -storepass $STOREPASS -keypass $SIGNKEYPASS -dname "$KEY_DN"
 
		$KEYTOOL -exportcert -v -alias $SIGNKEYNAME -keystore $KEYSTORE -rfc -file $PEM_SIGN -storepass $STOREPASS
 
 
 	- submit public certificate to eID server for association with your account
 
 	- receive eID server Web-Service URL to use in your configuration
 	- receive eID web service certificates (enter in your truststore)
 
 	- create truststore file containing eID server certificate
 	- adjust configuration in fidelio-test1.properties or adequate renamed file
 
	Jun 28, 2017 7:05:33 PM org.apache.catalina.core.ApplicationContext log
	INFORMATION: configured config file: /fidelio-test1.properties
	Jun 28, 2017 7:05:33 PM org.apache.catalina.core.ApplicationContext log
	INFORMATION: using config file: /srv/appsrv2/fidelio-test1.properties

## Sample Guide

	FIDELIO sample instructions
	
	1. copy the contents of "sample-conf" into your Tomcat server directory
		- shared/lib contains the dependencies for CBOR and the eID connector
		- fidelio-test1.properties is already configured for eid.vx4.net
		- a matching host certificate and subject URL has to be supplied for
		  the entitlement certificate
		
	2. deploy fidelio.war to your server either through management web interface
	   or copy it into the webapps folder of your server installation
	   
	3. check service by visiting https://yourhost/fidelio/
	
	4. check logs for errors (unusual incompatibilities, outdated libraries)
	
	5. conf-fidelio-test1 contains a sample bsi-fidelio.crt, thats the client
	   certificate to be used for TLS authentication and web service signature.
	   Configure this certificate at the eID server to be used as authentication
	   certificate.
	   
	6. a sample truststore is provided containing the certificate for VX4, adjust
	   the truststore settings to match your eID servers signing certificate.

## Configuration 

	Configuration steps
	
	1. eID service 
		- generate keystore
		    ->  see create client keypair
		
		- create truststore
		    The truststore should be provided by the eID-service as PKCS#12 or JKS file.
		    You can create your own truststore and add an individiual certificate as follows:

 		    KEYTOOL=$JAVA/bin/keytool
		    $KEYTOOL -importcert -keystore $TRUSTSTORE -file cert.der
		    
		    If the certificate is provided as BASE64 encoded PEM convert it with:
		    openssl x509 -inform DER -outform PEM -in cert.pem -out cert.der
		    
		- get eService URL
		
		- create eServiceSecret
		    openssl rand -hex 32

	2. FIDO attestation certificate
		- generate
                U2F reference test-vectors can be used as well as custom attestation key-pairs 
                could be provided on request. You can also create your own.
		- copy in installation folder on server
		- include in config
	
	3. FIDELIO global counter with redis
	    - install redis
	    - configure redis installation (IP, port and password)
	    - include in config

	4. FIDELIO well-known applications
	    - adjust well-known application hashes in config-file

### Layout of cofiguration file

URL of eID web-service, to be comunicated by your provider

    eid.eIdServiceDestination=https://eid.vx4.net/eid/ws/eID

Your client key-store containing the authentication certificate

    eid.keystoreType = PKCS12
    eid.keystoreLocation = ${confRoot}/conf-fidelio-test1/bsi-fidelio.p12
    eid.keystorePassword = 4plf1jYpl5cciUVCjW6UF0mU8jdsGzGD
    eid.signatureKeyAlias = fidelio-test1
    
In case of a JKS store with a different key passwor    
    
    #eid.signatureKeyPassword =

The truststore containing the server SOAP certificate

    eid.truststoreType=JKS
    eid.truststoreLocation = ${confRoot}/conf-fidelio-test1/VX4-eIDSOAP-trust.jks
    eid.truststorePassword = changeit

***CHANGE!*** eService specific value to diversify the RID returned by the eID-server (hex-string, 32 bytes)

    eid.eServiceSecret = 290d37c7bff13d9802789db7354fd28ad563eab3ec5dcafa18608255a2cf57d1


#### FIDELIO eService specific parameters


Attestation key, fixed to PKCS12, one ECDSA key + certificate

    fidelio.attestationKeystore = ${confRoot}/conf-fidelio-test1/vendor-signer-key.p12
    fidelio.attestationPassword = Z0oPQSs+cEkUrix7ge0iya9c4s5KxIrw

Redis server, i.e. redis://<password>@localhost:6379 with counter prefix _FGC1/_ and _256_ counters

    fidelio.redisServer = redis://localhost:6379
    fidelio.redisCounterPrefix = FGC1/
    fidelio.redisCounterOrder = 256

Well-known applications

    fidelio.appId.A54672B222C4CF95E151ED8D4D3C767A6CC349435943794E884F3D023A8229FD=Google
 
## Build

	
* Prerequisites:
	
	JDK8, Maven 3.x, IDE if code modifications required
	
* IDE:

	Eclipse: Import as project from project settings into MARS.2 or later
	  
 	Other IDE: import from Eclipse settings if available or from Maven settings
 

* Build step to execute in project main directory (where pom.xml resides):
 
	mvn clean install
 
Result is located in target/fidelio.war as already renamed WAR-file.
Additionally the package is installed in the users local maven repository
as eService-<version>.war.
  
## Known issues / TODOs
  
  * Debian: /usr/share/tomcat7 vs. /var/lib/tomcat7 in INSTALL GUIDE sample/guidance 

## Development

* The project was started in Eclipse MARS.2 and created as Maven project. Any IDE capable to handle maven projects will
do.  

## FIDELIO eService polyfill
As another option besides the FIDELIO Chrome extension a mostly browser independent (Chrome, Firefox, Opera, Edge and
Safari (Safari not very well tested at the time of writing)) Javascript protocol wrapper exists.

To embed FIDELIO functionality into web front ends a JavaScript API extension (so called polyfill) is provided with
the service. Currently [WebAuthn WD-07](https://github.com/w3c/webauthn/milestone/13) and FIDO U2F are supported.
The decision for a polyfill had been made because it is a major overhead to provide up-to-date browser extensions
for multiple browsers while targeting a heavily work-in-progress API.

The respective up-to-date polyfill is available as part of the FIDELIO e-Service. The official FIDELIO test instance
provides the polyfill under the URL
[https://id1.vx4.eu/fidelio/js/fidelio.js](https://id1.vx4.eu/fidelio/js/fidelio.js).

### Embedding vs. Injection

 * Option 1: the relying party includes the FIDELIO polyfill to support WebAuthn and U2F with the German eID by
 including a direct reference to the JavaScript. Ideally using a chosen and incremented parameters within the URL to
 a) allow caching and b) upgrading the caches if the polyfill changes.<br/>
 I.e. embed the polyfill with a script tag in the head of your rendered pages<br/>
 ```<script type="text/javascript" src="https://id1.vx4.eu/fidelio/js/fidelio.js"></script>```

 Please keep in mind if you used U2F with classical FIDO tokens before you'll probably have some entry like <br/>
  ```<script type="text/javascript" src="https://.../u2f-api.js"></script>``` in your page. Please ensure either to
  use two different pages for classic U2F login and FIDELIO U2F login as those two APIs do not interact very well at
  the moment or place the FIDELIO script tag behind (at the end) of your script declarations and ensure classic U2F
  loads before FIDELIO. Classic U2F will overwrite any other API and it is a known issue currently that calls from
  FIDELIO in case no FIDELIO keyHandle has been found or no eID-Client was started to the original API seem not to work
  properly at least in Chrome.
 
 * Option 2: As a user you may find it convenient to add FIDELIO WebAuthn and U2F functionality forcefully to web pages
 you visit. In this case a [Tampermonkey](https://tampermonkey.net/) user script is provided under the URL
 [https://id1.vx4.eu/fidelio/js/fidelio.user.js](https://id1.vx4.eu/fidelio/js/fidelio.user.js). After Tampermonkey is
 active in your browser just clicking the link will start the installation dialog. Well-known test URLs have already
 been added to match the script extension. In case you want to match *any* website go to the Tampermonkey overview
 screen, select the "FIDELIO Injector" script, click "Settings" and add the following two string as user matches:<br/>
 ```https://*``` and ```https://*/*```

Same warnings apply as for option #1. The main difference between WebAuthn and FIDO is that WebAuthn comes with the
browser while U2F was always added with a specific piece of API to access browser extensions. If the target website
includes its own u2f-api.js it will highly depend on the loading order of the browser which API gets activated. It is
generally a good idea to load FIDELIO as late as possible. But a few exceptions exist where jQuery based sites check
for U2F support as soon as the embedded script parts execute. Depending on browser behavior those web sites might not
be usable with the injected FIDELIO API.

### Inside the polyfill

Developers trying to understand the content of the FIDELIO polyfill will find some explanations in the following
section. We decided not to include source-code documentation in the Javascript as those (useless from a machines
point of view) comments will be transmitted every time a browser requests the current polyfill.
 
In the very beginning the code sets ```window.PublicKeyCredential``` to an empty object if not existent to convince
testcases WebAuthn with public-key credentials is supported. This is done as some implementations require this behaviour
to detect WebAuthn support successfully.

Afterwards the script searches its own script-tag to determine its own base address. Thats why you should always
include the script from the service you are using. Changing the target URL manually to a certain FIDELIO eService
while the script is hosted somewhere else without knowing what you're doing might work in the first place but will soon
fail if protocol details are adjusted.

Furthermore the script uses lazy loading relatively to this base URL to add required components such as CBOR.

Because Chrome on Android behaves quite different when having to use intents to start an application on the phone
efforts had been made to deal with Chrome that's why the polyfill distinguishes calling methods in this case. Due to
continued complications it was decided to deactivate polyfill support on Android as the FIDELIOApp integration works
very stable and quite well with all so far encountered relying parties. A future option might be translation between
WebAuthn and U2F and providing a FIDELIO WebAuthn and a FIDELIO U2F polyfill as a base. As it was discussed Chrome
might add such a translating bridge internally this wasn't followed until there is a specific demand.

Utility functions and protocol encodings are mostly identical to the Chrome extension. So everything else is based on
the fidelioEN (enrollment, registration) and fidelioAU (authenticate, sign(in), login) functions:

    var fidelioEN = function fidelioEN(appIdHash, clientDataHash, blackList, cb) {
        callService(1, {1 : appIdHash, 2 : clientDataHash, 5 : blackList}, cb); };
    var fidelioAU = function fidelioAU(appIdHash, clientDataHash, whiteList, cb) {
        callService(2, {1 : appIdHash, 2 : clientDataHash, 3 : whiteList}, cb); };

Especially for WebAuthn eventually existing APIs are stored in case requests can not be fulfilled with FIDELIO. A
common case is EDGE with its lacking support for elliptic curves and thus preferring RSA algorithms which are considered
obsolete in terms of FIDELIO.

WebAuthn WD-07 support is injected with the implementations of the following two functions.

    navigator.credentials.create = function(options)
    navigator.credentials.get = function(options)

They are both implemented using promises to conform to the original API, both try to call the original API if
credentials don't match the FIDELIO requirements (ES256 and keyHandle / credential ID starts with 0xF1DE1101).

```credentials.create``` relies on ```fidelioEN``` while ```credentials.get``` internally uses ```fidelioAU```
to get the cryptographic parts done. The bottom parts of both functions deal with the creation of the binary
encoding for the WebAuthn compliant responses.

The U2F functionality is provided by injection of the functions:

    window.u2f.register = async function(appId, registerRequests, registeredKeys, callback, opt_timeoutSeconds)
    window.u2f.sign = async function(appId, challenge, registeredKeys, callback, opt_timeoutSeconds)

They benefit from the U2F signature layout used by the eService and need no further conversion.

### Important notes

Please note: FIDO U2F trusted facet list verification is not supported in the polyfill!

Please note: channel IDs for channel binding cannot not be accessed from the polyfill and therefore can not be included
in the client data structure.

Please note: As soon as the WebAuthn drafts stabilize the eService will generate appropriate WebAuthn responses thus
making the conversion code superfluous - which in turn will be subject to change and/or complete removal.

## License
#### European Union Public License 1.1

* See LICENSE.md
