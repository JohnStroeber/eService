Datenschutzerklärung FIDELIO 
============================

```
adesso AG
Adessoplatz 1
44269 Dortmund

Version 1.2
vom 01.11.2018
```

Inhaltsverzeichnis
------------------

[Glossar](#glossar)

[Dokumentenhistorie](#dokumentenhistorie)

[1. Präambel](#1-präambel)

[2. Datenerhebung und –verarbeitung](#2-datenerhebung-und-verarbeitung)

[3. Nutzung und Weitergabe personenbezogener Daten](#3-nutzung-und-weitergabe-personenbezogener-daten)

[4. Freigaberegelungen](#4-freigaberegelungen)

[5. Regelung der Abrufverfahren von personenbezogenen Daten](#5-regelung-der-abrufverfahren-von-personenbezogenen-daten)

[6. Regelung der Auftragsdatenverarbeitung von personenbezogenen Daten](#6-regelung-der-auftragsdatenverarbeitung-von-personenbezogenen-daten)

[7. Regelung zur Aufrechterhaltung des Datenschutzes](#7-regelung-zur-aufrechterhaltung-des-datenschutzes)

[8. Regelung zur Datenlöschung / Vernichtung](#8-regelung-zur-datenlöschung-vernichtung)

[9. Sicherheit](#9-sicherheit)

[10. Auskunftsrecht und Kontaktdaten](#10-auskunftsrecht-und-kontaktdaten)

[11. Gültigkeit und Aktualität der Datenschutzerklärung](#11-gültigkeit-und-aktualität-der-datenschutzerklärung)

Glossar
-------

| **Abkürzung** | **Beschreibung** |
| ------------- | ---------------- |
| Challenge | Challenge-Response-Verfahren der Relying Party |
| DSB | Datenschutzbeauftragter |
| DSGVO | Datenschutz-Grundverordnung |
| FIDELIO eService | elektronischer / web-basierter Authentifizierungsdienst für die Kopplung der eID mit anderen Authentifizierungsprotokollen |
| FIDO | **F** ast **ID** entity **O** nline, größtes Ökosystem für standardbasierte interoperable Authentisierung, [https://www.fidoalliance.org](https://www.fidoalliance.org) |
| Relying Party | Web-Applikation die eine Nutzer Authentifizierung anfordert |
| RID | **R** estricted **ID** entity, dienste- und kartenspezifisches Merkmal, „Pseudonym&quot; |
| TMG | Telemediengesetz |

Dokumentenhistorie
------------------

| **Datum** | **Version** | **Kommentar** | **Autor** |
| --------- | ----------- | ------------- | --------- |
| 2018-11-01 | V 1.2 | Aktualisierung der Anschrift des Hauptsitzes | [Christian Kahlo](mailto:kahlo@adesso.de) |
| 2018-07-13 | V 1.1 | Review, Formatierung, Abgleich der Nomenklatur und Klarstellungen mit neuestem Stand der zugehörigen Dienstbeschreibung in Kapitel 2 | [Christian Kahlo](mailto:kahlo@adesso.de) |
| 2018-03-01 | V 1.0 | Überprüfung der Kapitel, Abschluss Erstentwurf | Markus Krebs |
| 2018-02-28 | V 0.9 | Kapitel 1-11 | Markus Krebs |

## 1. Präambel

Die adesso AG, vertreten durch die Geschäftsführer, nimmt den Schutz
Ihrer personenbezogenen Daten sehr ernst. Wenn Sie den FIDELIO-Dienst
nutzen, verarbeitet die adesso AG Daten im Rahmen der gesetzlichen
Bestimmungen. Maßgebliche Vorschriften dazu enthält das
Telemediengesetz.

Im Folgenden wird erläutert, welche Daten während Ihrer Nutzung des
FIDELIO-Dienstes erfasst und verwendet werden.

## 2. Datenerhebung und –verarbeitung

Wenn Sie den FIDELIO-Dienst nutzen und in die Nutzungsvereinbarung von
FIDELIO und damit nach Art. 6 Abs. 1 lit. a DSGVO einwilligen, werden
Daten von Ihrem Ausweisdokument erhoben. Bei den Daten handelt es sich
um das sog. Pseudonym oder dienste- und kartenspezifische Merkmal (eine
32 Byte lange Kennung, auch RID genannt), welches aus dem elektronischen
Personalausweis oder elektronischem Aufenthaltstitel ausgelesen wird,
eine sektorspezifische Wiedererkennbarkeit einer Person ermöglicht, und
somit als personenbezogenes Merkmal gelten kann. Ein Umkehrschluss auf
eine konkrete Person ist nur mit Kenntnis des Pseudonyms allein nicht
möglich. Die Daten werden zur Ableitung des Schlüsselmaterials für die
Erstellung und Prüfung einer Signatur von Challenges und die Erzeugung
von FIDO-Schlüsselpaaren erhoben, dabei eine juristische Sekunde lang
gehalten und danach durch aktives Überschreiben aus dem flüchtigen
Arbeitsspeicher gelöscht. Zweck ist es die FIDO-Enrollment und
FIDO-Authentication Anfragen auf Verlangen des Nutzers zu beantworten.
Bei Erhebung des Pseudonyms findet bei Entgegennahme der Daten durch den
FIDELIO-Dienst eine unumkehrbare Umschlüsselung desselbigen statt. Die
RID und die erzeugte Ableitung werden NICHT gespeichert oder in
Log-Dateien ausgegeben.

Im weiteren Verlauf werden daher keine personenbezogenen Daten
verarbeitet.

## 3. Nutzung und Weitergabe personenbezogener Daten

Jegliche Nutzung Ihrer personenbezogenen Daten erfolgt nur zu den
genannten Zwecken und in dem zur Erreichung dieser Zwecke erforderlichen
Umfang.

Übermittlungen personenbezogener Daten an Dritte findet nicht statt.

## 4. Freigaberegelungen

Vor der Freigabe von IT-Verfahren wird generell eine
datenschutzrechtliche Prüfung durchgeführt, bei FIDELIO werden
personenbezogene Daten im engeren Sinne nicht bearbeitet. Für
Software-Tests werden technisch fiktive Testausweise verwendet, die
keine gesonderte Freigabe benötigen.

## 5. Regelung der Abrufverfahren von personenbezogenen Daten

Sie entscheiden selbst darüber, welche abrufende Stelle Zugang zu dem
abgeleiteten Schlüsselmaterial erlangt. Im Abrufverfahren nach FIDO ist
der Vorgang, dass alle datenschutzrechtlichen Rahmenbedingungen
eingehalten sind, nicht kontrollierbar.

## 6. Regelung der Auftragsdatenverarbeitung von personenbezogenen Daten

Sie entscheiden selbst darüber, wer der erste Auftragnehmer (Relying
Party) ist und werden über die relevanten Prozesse und Datenelemente
aufgeklärt. FIDELIO ist der zweite Auftragnehmer bzw. der
Erfüllungsgehilfe für die Auftragsdatenverarbeitung. Eine Kontrolle des
ersten Auftragnehmers, also der Relying Party, im Sinne von FIDO ist
durch FIDELIO dabei nicht möglich. Steht der Auftragnehmer im direkten
Zusammenhang mit dem FIDELIO-Betreiber, also der adesso AG, sind dessen
Mitarbeiter zur Einhaltung des Datengeheimnisses verpflichtet.

## 7. Regelung zur Aufrechterhaltung des Datenschutzes

Es erfolgt eine regelmäßige Überprüfung durch den betrieblichen DSB,
während eine Überprüfung durch Landes- oder Bundes-Datenschützer möglich
ist, aber nicht pauschal vorausgesetzt werden kann.

## 8. Regelung zur Datenlöschung / Vernichtung

Der DSB kontrolliert die datenschutzgerechte Löschung von Datenträgern.
Ein Datenträger im Sinne von nichtflüchtigen Datenträgern existiert
nicht. Personenbezogene Daten werden bei der Schlüsselableitung nur im
flüchtigen Speicher gehalten und unmittelbar nach der Verarbeitung
gelöscht.

## 9. Sicherheit

Die adesso AG setzt technische und organisatorische Sicherheitsmaßnahmen
ein, um personenbezogenen Daten gegen zufällige oder vorsätzliche
Manipulationen, Verlust, Zerstörung oder gegen den Zugriff
unberechtigter Personen zu schützen. Die Sicherheitsmaßnahmen werden
entsprechend der technologischen Entwicklung fortlaufend verbessert.

## 10. Auskunftsrecht und Kontaktdaten

Ihnen steht ein Auskunftsrecht bezüglich der über Sie gespeicherten
personenbezogenen Daten, das Recht auf Berichtigung unrichtiger Daten,
Sperrung und Löschung zu. Derzeit existieren keine
technisch-organisatorischen Verfahren, um die Rechte der Betroffenen bei
der Verarbeitung personenbezogener Daten zu wahren, da keine
personenbezogenen Daten im klassischen Sinne erhoben oder gespeichert
werden. Wenn Sie Auskunft über Ihre personenbezogenen Daten
beziehungsweise deren Korrektur oder Löschung wünschen oder
weitergehende Fragen über die Verwendung Ihrer uns überlassenen
personenbezogenen Daten haben, kontaktieren Sie bitte den
Datenschutzbeauftragten.

```
Julius Hüttmann
Adessoplatz 1
44269 Dortmund

huettmann@adesso.de
+49 231 7000-2280
```

## 11. Gültigkeit und Aktualität der Datenschutzerklärung

Mit der Nutzung des FIDELIO-Dienstes willigen Sie in die vorab
beschriebene Datenverarbeitung ein. Die Datenschutzerklärung ist aktuell
gültig und datiert vom **01.11.2018**. Durch die Weiterentwicklung
unseres Dienstes oder die Implementierung neuer Technologien kann es
notwendig werden, diese Datenschutzerklärung zu ändern. Die adesso AG
behält sich vor, die Datenschutzerklärung jederzeit mit Wirkung für die
Zukunft zu ändern. Wir empfehlen Ihnen, sich die aktuelle
Datenschutzerklärung von Zeit zu Zeit erneut durchzulesen.
